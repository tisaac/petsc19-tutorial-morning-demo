
import sys
import petsc4py

petsc4py.init(sys.argv) # Ensures that command line that configure global PETSc
                        # behavior are used to initialize the PETSc environment

from petsc4py import PETSc
from mpi4py import MPI

N =  100 # Size of the vector space
         
nnz = 10 # Number of nonzeros in the sparse vector
         # TODO: Use the options database to make this size configurable from
         # the command line
 
# Create a full-sized vector
v = PETSc.Vec().create(comm=PETSc.COMM_WORLD)
v.setSizes([PETSc.DETERMINE, N])
v.setUp()

prand = PETSc.Random().create(comm=PETSc.COMM_WORLD) # What kind of random number generator do we get?
v.setRandom(random=prand)

v.view() # We always view the vector, which creates noisy input: can we make this optional?

# Get the ownership range of this process
vlo,vhi = v.getOwnershipRange()

# Get the number of random values to generate on this process
my_nnz,_ = PETSc.Sys.splitOwnership(nnz, comm=PETSc.COMM_WORLD)

prand.setInterval((vlo,vhi))
# create some random indices
idces = sorted([int(prand.getValue()) for i in range(my_nnz)])

# Use the indices to create a VecScatter, taking values from
# the full-length vector to the packed random indices
idces = PETSc.IS().createGeneral(list(idces),comm=PETSc.COMM_WORLD)

idces.view()

# Get the packed values from the full length vector
vpacked = PETSc.Vec().createMPI([my_nnz, nnz],comm=PETSc.COMM_WORLD)
vpacked.setUp()

sparse2packed = PETSc.Scatter().create(v, idces, vpacked, None)

sparse2packed.view()

sparse2packed.scatter(v, vpacked, PETSc.InsertMode.INSERT)

vpacked.view()

vpackedlo,vpackedhi = vpacked.getOwnershipRange()

# Make a VecScatter the will redundantly copy vpacked on each process
scatter2all, vredundant = PETSc.Scatter.toAll(vpacked)
scatter2all.view()
vredundant.view()
scatter2all.scatter(vpacked,vredundant)
vredundant.view()

# Get the raw index array of the sparse indices
idcesAll = idces.allGather()
idcesRaw = idces.getIndices()
idcesAllRaw = idcesAll.getIndices()

mat = PETSc.Mat().create(comm=PETSc.COMM_WORLD)
mat.setSizes((v.sizes, v.sizes))
mat.setUp()
for i in range(vpackedlo,vpackedhi):
    for j in range(nnz):
        mat[idcesRaw[i-vpackedlo],idcesAllRaw[j]] = vpacked[i] * vredundant[j]
mat.assemble()
mat.shift(1.)
mat.view()

x = v.duplicate()
b = v.duplicate()
b.setRandom()
b.view()

ksp = PETSc.KSP().create(comm=PETSc.COMM_WORLD)
ksp.setOperators(mat)
ksp.solve(b,x)
x.view()



