Petsc'19 Morning Tutorial Demo
==============================

Solve :math:`(I + v v^T)x = b`, where :math:`v` is a sparse vector.

